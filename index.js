/*
  Ejercicio: Creación de cliente y servidor en Node.js
  Autor: Veselin Georgiev | veselingp@hotmail.com
  Descripción: API REST sencilla que almacena datos de una persona
*/
// Inicializamos módulos a utilizar
var express = require('express');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var IP = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';
var PORT = process.env.OPENSHIFT_NODEJS_PORT || 3030;
var app = express();
// Agregamos configuración
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(bodyParser.json());
app.use(methodOverride());
app.use('/api', require('./rutas/api.js'));
app.listen(PORT, IP);
console.log('Servidor ejecutándose en: http://' + IP + ':' + PORT + '/');