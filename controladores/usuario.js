// Inicializamos módulos a utilizar
var lokijs = require('lokijs');
var db = new lokijs('db.json');
var usuario = db.addCollection('usuario');
// Creamos funciones del controlador
exports.getUsuarios = function(req, res){ //GET
	usuarios = usuario.data;
	if(!usuarios)
		res.status(404).jsonp({'message': 'Usuarios no encontrados'});
	else
		res.status(200).jsonp(usuarios);
};
exports.postUsuario = function(req, res){ //POST
	usuarios = usuario.insert({
		nombre: req.body.nombre,
		edad: req.body.edad
	});
	if(!usuarios)
		res.status(404).jsonp({'message': 'Usuario no agregado'});
	else
		res.status(200).jsonp(usuarios);
};
exports.getUsuario = function(req, res){ //GET
	usuarios = usuario.get(parseInt(req.params.id, 10));
	if(!usuarios)
		res.status(404).jsonp({'message': 'Usuario no encontrado'});
	else
		res.status(200).jsonp(usuarios);
};
exports.putUsuario = function(req, res){ //PUT
	usuarios = usuario.get(parseInt(req.params.id, 10));
	if(!usuarios)
		res.status(404).jsonp({'message': 'Usuario no encontrado'});
	else{
		if(req.body.nombre)
			usuarios.nombre = req.body.nombre;
		if(req.body.edad)
			usuarios.edad = req.body.edad;
		if(req.body.nombre || req.body.edad)
			usuarios = usuario.update(usuarios);
		res.status(200).jsonp(usuarios);
	}
};
exports.deleteUsuario = function(req, res){ //DELETE
	usuarios = usuario.get(parseInt(req.params.id, 10));
	if(!usuarios)
		res.status(404).jsonp({'mensaje': 'Usuario no encontrado'});
	else{
		usuarios = usuario.remove(parseInt(req.params.id, 10));
		res.status(200).jsonp({'mensaje': 'Usuario eliminado'});
	}
};